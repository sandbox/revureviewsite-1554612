<?php
/**
 * @file
 * Provides any views handler file.
 */

/**
 * Implementation of hook_uninstall().
 */
class ViewsHandlerFieldNodeRevuComments extends views_handler_field {
  /**
   * This is init methord.
   */
  public function init(&$view, $options) {
    parent::init($view, $options);
  }
  /**
   * When rendering the field.
   */
  public function render($values) {
    // Ensure Revu comments are available.
    $node = node_load($values->nid);
    if (user_access('view revu comments') && isset($node->revu)) {
      return theme('revu_comments_num', $node->revu['domain'], $node->revu['url']);
    }
  }
}
