<?php
/**
 * @file
 * Provides any required installation or upgrade path requirements.
 */

/**
 * Implements hook_views_data_alter().
 */
function revu_views_data_alter(&$data) {
  // Number of Revu comments made on the given node.
  $data['node']['revu_comments'] = array(
    'field' => array(
      'title' => t('Revu: Comments'),
      'help' => t('The number of Revu comments made on the node. Note that this will not work in the preview.'),
      'handler' => 'views_handler_field_node_revu_comments',
    ),
  );
}

/**
 * Implements hook_views_handlers().
 */
function revu_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'revu') . '/include',
    ),
    'handlers' => array(
      // Field handlers.
      'views_handler_field_node_revu_comments' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}
