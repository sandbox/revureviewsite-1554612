<?php
/**
 * @file
 * Provides any required installation or upgrade path requirements.
 */

/**
 * class revu.
 */
class Revu {
  public $UserApiKey = NULL;
  public $ForumApiKey = NULL;
  public $ApiUrl = 'http://www.re-vu.com/api/';
  public $ApiVersion = '1.1';

  /**
   * Creates a new interface to the Revu API.
   */
  public function __construct($user_api_key = NULL, $forum_api_key = NULL, $api_url = 'http://www.re-vu.com/api/') {
    $this->UserApiKey = $user_api_key;
    $this->ForumApiKey = $forum_api_key;
    $this->ApiUrl = $api_url;
  }

  /**
   * Validate API key and get username.
   */
  public function GetUserName() {
    return $this->call('GetUserName', array(), TRUE);
  }

  /**
   * Get a forum API key for a specific forum.
   */
  public function getForumApiKey($forum_id='') {
    return $this->call('get_ForumApiKey', array('forum_id' => $forum_id));
  }

  /**
   * Returns an array of hashes representing all forums the user owns.
   */
  public function GetForumList() {
    return $this->call('get_forum_list');
  }

  /**
   * Get a list of comments on a website.
   */
  public function GetForumPosts($forum_id='', array $options = array()) {
    $options['forum_id'] = $forum_id;
    return $this->call('get_forum_posts', $options);
  }

  /**
   * Count a number of comments in articles.
   */
  public function GetNumPosts($thread_ids = array()) {
    $thread_ids = implode(',', $thread_ids);
    return $this->call('get_num_posts', array('thread_ids' => $thread_ids));
  }

  /**
   * Get a list of threads on a website.
   */
  public function GetThreadList($forum_id, $limit = 25, $start = 0) {
    return $this->call('get_thread_list', array(
    'forum_id' => $forum_id,
    'limit' => $limit,
    'start' => 0));
  }

  /**
   * Get a list of threads with new comments.
   */
  public function GetUpdatedThreads($forum_id, $since) {
    return $this->call('get_updated_threads', array('forum_id' => $forum_id, 'since' => $since));
  }

  /**
   * Get a list of comments in a thread.
   */
  public function GetThreadPosts($thread_id, $options = array()) {
    $options['thread_id'] = $thread_id;
    return $this->call('get_thread_posts', $options);
  }

  /**
   * Get or create thread by identifier.
   */
  public function ThreadByIdentifier($identifier, $title) {
    return $this->call('thread_by_identifier', array('title' => $title, 'identifier' => $identifier), TRUE);
  }

  /**
   * Get thread by URL.
   */
  public function GetThreadByUrl($url, $partner_api_key = NULL) {
    return $this->call('get_thread_by_url', array('url' => $url));
  }

  /**
   * Updates thread.
   */
  public function UpdateThread($thread_id, array $options = array()) {
    $options['thread_id'] = $thread_id;
    return $this->call('update_thread', $options, TRUE);
  }

  /**
   * Creates a new post.
   */
  public function CreatePost($thread_id, $message, $author_name, $author_email, array $options = array()) {
    $options['thread_id'] = $thread_id;
    $options['message'] = $message;
    $options['author_name'] = $author_name;
    $options['author_email'] = $author_email;
    return $this->call('create_post', $options, TRUE);
  }

  /**
   * Delete a comment or mark it as spam (or not spam).
   */
  public function ModeratePost($post_id, $action) {
    return $this->call('create_post', array('post_id' => $post_id, 'action' => $action), TRUE);
  }

  /**
   * Makes a call to a Revu API method.
   */
  public function call($arguments = array(), $post = FALSE) {
    // Construct the arguments.
    $args = '';
    if (!isset($arguments['UserApiKey'])) {
      $arguments['UserApiKey'] = $this->UserApiKey;
    }
    if (!isset($arguments['ForumApiKey'])) {
      $arguments['ForumApiKey'] = $this->ForumApiKey;
    }
    if (!isset($arguments['api_version'])) {
      $arguments['api_version'] = $this->api_version;
    }
    foreach ($arguments as $argument => $value) {
      if (!empty($value)) {
        $args .= $argument . '=' . urlencode($value) . '&';
      }
    }

    // Call the Revu API.
    $ch = curl_init();
    if ($post) {
      curl_setopt($ch, CURLOPT_URL, $this->ApiUrl . $function . '/');
      curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
      curl_setopt($ch, CURLOPT_POST, 1);
    }
    else {
      curl_setopt($ch, CURLOPT_URL, $this->ApiUrl . $function . '/?' . $args);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    $data = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);

    // Check the results for errors (200 is a successful HTTP call).
    if ($info['http_code'] == 200) {
      $revu = json_decode($data);
      if ($revu->succeeded) {
        // There weren't any errors, so return the results.
        return isset($revu->message) ? $revu->message : NULL;
      }
      else {
        throw new RevuException(isset($revu->message) ? $revu->message : NULL, 0, $info, $revu);
      }
    }
    else {
      throw new RevuException('There was an error querying the Revu API.', $info['http_code'], $info);
    }
  }
}

/**
 * Any unsucessful result.
 */
class RevuException extends Exception {
  /**
   * The information returned from the cURL call.
   */
  public $info = NULL;

  /**
   * The information returned from the Revu call.
   */
  public $revu = NULL;

  /**
   * Creates a RevuException.
   */
  public function __construct($message, $code = 0, $info = NULL, $revu = NULL) {
    $this->info = $info;
    $this->revu = $revu;
    parent::__construct($message, $code);
  }

  /**
   * Converts the exception to a string.
   */
  public function __toString() {
    $code = isset($this->revu->code) ? $this->revu->code : (isset($info['http_code']) ? $info['http_code'] : 0);
    $message = $this->getMessage();
    return __CLASS__ . ": [$code]: $message\n";
  }
}
